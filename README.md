# **Facade Pattern** #




###Description###
Facade pattern is a pattern that make the calling of many class to use that have complicated process easier.This pattern's name is come from the decoration of the face of the building.The facade need to be decorate to conceal the real face that may be unattractive. Same as this design pattern. This design pattern is created to hide the complex or difficult to understand and create a new one that is friendly to client to use.The client don't need to know the inside complicated code,they just use a instant code from facade.
###Advantage of this design pattern###
* provide software easier to use
* reduce the complicated of the code

###Usage###
![facade.png](https://bitbucket.org/repo/k9prj5/images/370821299-facade.png)

if we have a group of class that has the complex using. We can create a class as a facade to hide the complicated process of code.So, the client just use the method from the facade class without need of knowing the process.

###Example###
This is an example of how a client start the car engine.
```
#!java
public class CarEngine {
	public void startTheEngine(){
		System.out.println("Engine is stated");
	}
}


public class Battery {
	public void powerTheCar(){
		System.out.println("Power the car system");
	}
}


public class StarterMotor {
	public void startMotor(){
		System.out.println("Motor is started");
	}
}

```

if we didn't use the facade pattern the client need to do everything with himself/herself like this one.

```
#!java

public class Main {
	public static void main(String arg[]){
		
		//Create a car component
		CarEngine engine = new CarEngine();
		Battery battery = new Battery();
		StarterMotor motor = new StarterMotor();
		
		//start all of the component
		engine.startTheEngine();
		motor.startMotor();
		battery.powerTheCar();

	}
}


```

Let implement the Facade pattern by create a facade class.

```
#!java


public class CarFacade {
	private Battery battery;
	private CarEngine engine;
	private StarterMotor motor;
	
	public CarFacade(){
		battery = new Battery();
		engine = new CarEngine();
		motor = new StarterMotor();
	}
	
	public void startCar(){
		battery.powerTheCar();
		engine.startTheEngine();
		motor.startMotor();
	}
}

```
After we already create the facade class the client only need to use the method startCar from the facade class only,so it will more friendly to use than the old one.

```
#!java


public class Main {
	public static void main(String arg[]){
		
		//Create a car facade
		CarFacade car = new CarFacade();
		
		//start the car
		car.startCar();
	}
}

```





As you can see,when we implement the facade pattern we remove the complex process and provide the client to start the car using only one method.



###Exercise###
Create a compute with many component using a facade method to hide the complex code using one method.
here is the component class of the computer.

```
#!java


public class Mainboard {
	public void setMainboard(){
		System.out.print("Mainboard is set");
	}
}

public class CPU {
	public void setCPU(){
		System.out.println("CPU is set");
	}
}

public class RAM {
	public void setRAM(){
		System.out.println("Ram is set");
	}
}


public class GraphicCard {
	public void setGraphicCard(){
		System.out.print("Graphic card is set");
	}
}



```





##Written by##
Pongsakorn Somsri 5710546321
Parisa Supitayakul 5710546313